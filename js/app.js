var myApp =  angular.module("myApp",['myAppService','angular.filter']);

myApp.controller("mainController",['$scope','appServiceData',function($scope,appServiceData){
 $scope.categoryArray = [];
  appServiceData.getInfo(function (response) {
       $scope.categoryArray = response;
    }, function (error) {
        console.log("error:", error);
    })

}])