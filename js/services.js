var myAppService = angular.module('myAppService', []);

myAppService.factory('appServiceData',['$http',function($http){

var serviceData={};

// this method calls the API and returns the Json object
serviceData.getInfo = function(callback){
    $http.get("http://agl-developer-test.azurewebsites.net/people.json")
    .success(function(data,status){
      callback(data);
    })
    .error(function(data,status){
       callback(data);
     })
}

return serviceData;
}]);